\documentclass[12pt]{article}

\usepackage{sbc-template}

\usepackage{graphicx,url}

\usepackage[brazil]{babel}   
\usepackage[utf8]{inputenc}  

     
\sloppy

\title{Algoritmos de Busca de Vizinhos em Diferentes Cenários de Distribuição Espacial}

\author{Bruno Normande Lins\inst{1}, Leonardo Pereira Viana\inst{2} }


\address{Mestrado em Modelagem Computacional do Conhecimento (MMCC)\\
	Universidade Federal Alagoas (UFAL)\\
	Maceió -- AL -- Brasil
\nextinstitute
  Instituro de Computação -- Universidade Federal Alagoas\\
  Universidade Federal Alagoas (UFAL)\\
  Maceió -- AL -- Brasil
  \email{normandelins@gmail.com, lpviana@lccv.com.br}
}

\begin{document} 

\maketitle

%\begin{abstract}
%  This meta-paper describes the style to be used in articles and short papers
%  for SBC conferences. For papers in English, you should add just an abstract
%  while for the papers in Portuguese, we also ask for an abstract in
%  Portuguese (``resumo''). In both cases, abstracts should not have more than
%  10 lines and must be in the first page of the paper.
%\end{abstract}
     
\begin{resumo} 
  Neste trabalho são analisados os principais algoritmos de detecção de contatos
  em relação ao seu
  comportamento em diferentes cenários de distribuição espacial. Em busca do
  conhecimento de como tais mudanças de cenários afetam os diferentes algoritmos,
  cada algoritmo será testado em cada um dos cenários com números crescentes de 
  partículas e seus tempo de execução medidos e comparados.
\end{resumo}


\section{Introdução}

Desde a formalização do Método dos Elementos Discretos (\textsf{DEM}) 
\cite{Cundall1988}, algoritmos de detecção de contatos têm sido um problema
em aberto: podendo chegar ocupar até 80\% do tempo total de processamento
\cite{Munjiza2004, Ericson2005} tal classe de algoritmo se torna um
verdadeiro gargalo nos sistemas em que precisam ser utilizados.

A paralelização de tais algoritmos não conseguiu baixar a proporção de tempo
gasta nessa etapa, mas conseguiu possível a simulação com 
\textsf{DEM} para milhões \cite{Markauskas2015}
e até centenas de milhões \cite{Fattebert2012} de partículas simultâneas.

O esforço para otimização dos sistemas que usam \textsf{DEM} está agora focado
no balanceamento dinâmico do espaço de busca \cite{Fattebert2012}.
Tal abordagem permite a utilização de diferentes tipos de algoritmos 
em cada situação. 
Os algoritmos tradicionais de
\textit{grid} já foram comparados com algoritmos de árvore \cite{Han2007} e 
já existem resoluções, para a queda no desempenho desses
algoritmos quando a diferença dos tamanhos das partículas são muito grandes
\cite{Perkins2001a, Araujo2007a}.

Esse trabalho busca responder a pergunta de quais algoritmos são mais recomendados
para os diferentes tipos de distribuições de partículas em uma simulação. Para
tal os principais algoritmos de \textit{grid} foram testados em diferentes tipos
de distribuições, com número crescente de partículas. Algoritmos de árvore não
foram incluídos pois seu desempenho já foi mostrado inferior em relação aos de 
\textit{grid} em cenários com partículas de tamanho homogêneo.

\section{Algoritmos de Deteção de Contato}

O algoritmo mais básico de detecção de contato é a checagem direta. Nesse
algoritmo, dado $N$ elementos de entrada, para cada elemento $N_i$ é
feito o cálculo de verificação de contatos com os outros $N - 1$ elementos.
A complexidade assintótica desse algoritmo é $\mathcal{O}(N^2)$, o que o 
torna impraticável
para sistemas em tempo real ou que possuam mais do que algumas centenas de corpos
interagindo.

A solução encontrada é realizar uma etapa conhecida como \textbf{Busca por Vizinhos}.
Nessa etapa primeiro é feito um mapeamento de todos os elementos do sistema para
em seguida ser verificado o contato apenas entre os elementos mais próximos, seus
vizinhos, eliminando assim a maioria dos testes do sistema \cite{Munjiza2004}.

Os algoritmos testados foram escolhidos por melhor representarem
os diferentes tipos de algoritmos de \emph{grid}, sendo um de mapeamento
direto e outros dois usando listas, com e sem ordenação. Além disso
esses algoritmos são apontados como os mais importante na literatura
\cite{Munjiza2004, Ericson2005}. São eles: Mapeamento Direto; Detecção de Contato por Ordenação; e NBS-Munjiza.


%\subsection*{Mapeamento Direto}
%
%O algoritmo de mapeamento direto usa uma matriz de listas encadeadas para
%representar o \textit{grid} de busca. Cada lista armazena os elementos que
%foram associados à célula que ela representa.

%\subsection*{Detecção de Contato por Ordenação}
%
%Esse algoritmo usa quatro vetores de tamanho $N$ para
%representar o \textit{grid} de busca. 
%Na etapa de mapeamento primeiro cada elemento tem seu identificador e suas 
%coordenadas salvas e então os vetores são ordenados a partir das coordenadas.
%
%Na etapa de procura por contato é preciso usar um algoritmo de busca 
%para encontrar os elementos das células testadas no vetor ordenado.
%
%\subsection*{NBS-Munjiza}
%
%O NBS-Munjiza (\emph{No Binary Search}) foi criado por Munjinza \cite{Munjiza1998}
%e busca representar o \textit{grid} sem usar uma matriz, que pode consumir muita
%memória, mas também sem precisar depender de algoritmos de ordenação ou busca 
%binária. Para tal ele faz uma varredura nos elementos e vai preenchendo as estruturas
%de dados à medida em que vai realizando a busca. O autor afirma obter desempenho
%similar ao Mapeamento Direto \cite{Munjiza2004}, mas utilizando menos memória.
%Seu maior problema é sua complexidade de implementação, inclusive
%esse foi o motivo dele ter ficado de fora dos testes de desempenho 
%de Han et al. \cite{Han2007}.

\section{Metodologia}

Os testes foram realizados usando o \emph{software} \textsf{DEMOOP}, implementado no
Laboratório de Computação Científica e Visualização (\textsf{LCCV}) na \textsf{UFAL}
e executados no \emph{cluster} computacional \textsf{GradeBR}.

Os casos de teste representam três cenários distintos de distribuição de partículas:
empacotamento denso, empacotamento esparso e empacotamento misto. Para cada
tipo de empacotamento os algoritmos foram executados com os mesmos casos de entrada,
com o número de partículas $N$ variando de $5\times 10^5$ até $5\times 10^6$.

Os resultados encontrados foram obtidos a partir da média aritmética do tempo de
execução de cada algoritmo para cada valor de $N$.


\section{Resultados}

A Figura \ref{fig:denso} mostra	 os resultados obtidos no cenário de empacotamento
denso. Podemos ver que nesse cenário o \textsf{NBS-Munjiza} obteve um desempenho
muito inferior em relação aos outros algoritmos, chegando a um comportamento
próximo ao quadrático. Por outro lado o algoritmo de Detecção de Contato por 
Ordenação, mesmo com as etapas de ordenação e busca binária, foi capaz de obter
um desempenho superior até mesmo ao mapeamento direto.

\begin{figure}[ht]
	\centering
	\includegraphics[width=.7\textwidth]{denso_results/result.png}
	\caption{Tempo de execução dos algoritmos com empacotamento denso}
	\label{fig:denso}
\end{figure}

Em média, o algoritmo \textsf{NBS-Munjiza} teve um desempenho de 194\%
sobre o algoritmo de Mapeamento direto. Já o algoritmo de Detecção por Ordenação
obteve uma melhora de 27\% no tempo obtido pelo Mapeamento Direto.


\section{Conclusão}

Os resultados obtidos até o momento se diferenciam bastante do 
esperado inicialmente. Em média o algoritmo \textsf{NBS-Munjiza} obteve tempo de
execução próximo ao dobro do Mapeamento Direto, 
se mostrando muito abaixo da expectativa no tipo
de cenário em que ele deveria possuir pelo menos uma eficiência similar ao 
Mapeamento Direto.
Isso pode se dar devido ao custo de limpar e reconstruir as listas usadas para
mapeamento durante toda a etapa de busca.
 
Já o algoritmo de Detecção por Ordenação se mostra superior,
precisando em média de apenas 72\% do tempo de execução necessário ao Mapeamento
Direto. O provável motivo de tal
melhora em desempenho é a simplicidade de suas estruturas usadas para o
mapeamento, frente às listas encadeadas necessárias ao outro algoritmo.

Para solidificar o resultado é necessário finalizar os testes nos outros cenários
de distribuição de partículas para obter mais conhecimento sobre o comportamento
desses algoritmos.
 

%\begin{figure}[ht]
%\centering
%\includegraphics[width=.5\textwidth]{fig1.jpg}
%\caption{A typical figure}
%\label{fig:exampleFig1}
%\end{figure}
%
%\begin{figure}[ht]
%\centering
%\includegraphics[width=.3\textwidth]{fig2.jpg}
%\caption{This figure is an example of a figure caption taking more than one
%  line and justified considering margins mentioned in Section~\ref{sec:figs}.}
%\label{fig:exampleFig2}
%\end{figure}


%\begin{table}[ht]
%\centering
%\caption{Variables to be considered on the evaluation of interaction
%  techniques}
%\label{tab:exTable1}
%\includegraphics[width=.7\textwidth]{table.jpg}
%\end{table}


\bibliographystyle{sbc}
\bibliography{reference}

\end{document}
