
\mychapter{\textsc{Detecção de Contatos}}{ch:fundamentacao}
\lhead{\textsc{Detecção de Contatos}}

Nesse capitulo serão introduzidos os conceitos essenciais a esse trabalho. Na
seção \ref{sec:dem} será apresentado o Método dos Elementos Discretos, que é 
o método da engenharia que é usado na implementação do \emph{software} desenvolvido
durante esse trabalho. Em seguida, na seção \ref{sec:algoritmos} é fundamentado o 
conceito de algoritmo de detecção de contatos e, mais detalhadamente, a
subseção \ref{sec:vizinhos} trata da técnica de busca por vizinhos.

\section{Método dos Elementos Discretos}\label{sec:dem}

O Método dos Elementos Discretos (\textit{Discret Elements Method}, \textsf{DEM}),
como definido por \citet{Cundall1992}, é um método numérico capaz de descrever
o comportamento mecânico de um conjunto de corpos rígidos. Para tal ele precisa
satisfazer duas condições:

\begin{itemize}
	\item Permitir o cálculo de deslocamento e rotações finitas independentes para
	corpos discretos;
	\item Reconhecer novos contatos automaticamente à medida em que ocorre
	a análise.
\end{itemize}

O tempo total da simulação é subdividido em pequenos passos de tempo ($\Delta t$), em cada
passo é feita a verificação de contato entre todos os corpos presentes na simulação, calculado 
a força dos contatos, feita a integração da equação de movimento e atualizada a posição dos corpos.
Esse processo é repetido para cada passo no tempo até que a simulação esteja completa como mostra
a Figura \ref{fig:demloop}.

\begin{figure}
	\centering	
	\includegraphics[width=0.8\linewidth]{figuras/demloop.png}
	\caption{Etapas realizadas em cada passo de tempo em uma simulação usando \textsf{DEM}.}
	\label{fig:demloop}
\end{figure}

O cálculo da força pode ser abordado de diferentes maneiras. Em geral a intensidade
da força de contato é uma função do quanto uma partícula interceptou a outra.
Para obter a direção da força a escolha do método utilizado pode depender da
geometria dos elementos do sistema assim como da complexidade de implementação 
desejada. Um dos métodos usados mais comuns na literatura é o método do plano
comum \citep{Cundall1988}.

Uma vez que temos a força que deverá ser aplicada em cada partícula, a aceleração
é obtida a partir da Segunda Lei de Newton (Equação \ref{eq:newton}).

\begin{equation} \label{eq:newton}
m \vec{a} = \vec{f}
\end{equation}
\listthisequation{\ref{eq:newton}}{Segunda Lei de Newton.}

A integração da equação de movimento pode ser realizada tanto analiticamente como
numericamente. Para problemas \textsf{DEM} o método numérico é mais adequado
por exigir menos hipóteses restritivas, serem mais versáteis e apresentarem
respostas aproximadas satisfatórias.

Os métodos explícitos são normalmente bastante adequados ao \textsf{DEM}, pela
característica de precisar apenas do estado em $t$ para o cálculo do novo
estado do sistema em $t + \Delta t$. Essa característica se adapta bem ao
ciclo mostrado na Figura \ref{fig:demloop} onde cada iteração representa um
incremento no tempo total de simulação.

Alguns autores escolhem utilizar métodos com amortecimento numérico devido
à imprecisões geradas pelas condições de contorno e pela própria característica
discreta da interação de contato, quando em um momento não há contato e abruptamente
passa a existir no próximo $\Delta t$. Esses métodos usam amortecimento numérico
para dissipar os erros gerados por essas imprecisões. Alguns exemplos
desses tipos de métodos são os algoritmos \textit{Meg-Alpha} \citep{Hulbert1996}
e Chung e Lee \citep{Chung1994} ambos comumente usados em \textit{softwares} 
\textsf{DEM} \citep{Silveira2001}.

É muito comum também o uso de métodos explícitos diretos. O método de Euler 
simplificado \citep{Green2013}
pode ser usado quando se deseja uma maior simplicidade de implementação. O método
das diferenças centrais também é bastante usado por apresentar baixa complexidade
de processamento e resultados satisfatórios. Outras variações desses métodos podem
ser usadas  ao se buscar melhoria na qualidade da análise \citep{Munjiza2004}.

%Mais simples e bastante eficiente computacionalmente é o método das diferenças
%centrais (\textsf{MDC}).

A abordagem mais comum atualmente para a paralelização dos softwares \textsf{DEM}
é o método da subdivisão do espaço de busca \citep{Cintra2016,Bokhari1987, karypis1999}.

Seja ele dinâmico ou estático, esse método consiste em separar grupos de partículas de acordo com sua
localização, assim cada grupo é separado de acordo com o algoritmo escolhido, como
por partições de grafos \emph{k-way} \citep{Markauskas2015} ou células Voronoi 
\citep{Fattebert2012} e assim cada \textsf{CPU}
fica responsável por uma região de busca. 
Esse método tem a vantagem de diminuir a quantidade
de comunicação entre as unidades de processamento, sendo ela necessária apenas
quando uma partícula sai de uma região para a outra. A desvantagem é que em cada uma
das \textsf{CPU}s executa na verdade o algoritmo sequencial. 

Apesar de adequada para \emph{clusters} de computadores, esse método de paralelismo
não é bem aplicado para o paralelismo que se obtêm com o uso de uma \textsf{GPU}. Nesses
casos, onde a comunicação não é tão cara, mas a memória dividida é limitada, 
\citet{Green2013} aponta uma outra abordagem,
a decomposição por partícula. Nesse tipo de decomposição os cálculos de cada partícula
é feito independentemente em cada unidade de processamento.

Essa divisão é ideal para as etapas de integração da equação de movimento e de atualização
das posições das partículas, visto que estas podem ser feitas independentemente
para cada partícula sem necessidade de informação dos outros elementos do sistema.

Já para o cálculo das interações de contato é preciso verificar para cada partícula
se ela está em contato com todas as outras. O custo computacional dessa checagem de
contato ingênua possui alto custo computacional, complexidade $O(N^2)$ e 
torna seu uso inviável em uma implementação DEM que for ser usada com valor de $N$ superior
à algumas centenas de milhares de elementos, mesmo se todo esse processamento for divido
entre os processadores de uma \textsf{GPU} \citep{Green2013}.

A solução usada é dividir a checagem de contato em dois passos. Para cada elemento $N_i$
primeiro são separados os elementos próximos a ele, também chamados de vizinhos. Então a 
verificação de contato em sí só precisa ser realizada para os elementos da lista de vizinhos
de cada partícula. A classe de algoritmos responsáveis por gerar essas listas são conhecidos
como Algoritmos de Busca por Vizinhos, mas primeiro vamos falar dos conceitos
gerais por trás da detecção de contato em sí \citep{Munjiza2004}. 

\section{Algoritmos de Detecção de Contatos} 
\label{sec:algoritmos}

Em sistemas com grande número de elementos, 
a etapa de busca por contato pode chegar a ocupar de 60\% a 80\% 
do tempo total de processamento \citep{Munjiza2004}.
Esse problema se torna ainda mais crítico se o sistema deve
ser calculado em tempo real \citep{Ericson2005}.

É importante entender que a verificação de contato entre dois corpos
é completamente dependente da geometria de ambos, podendo resulta em 
cálculos bastante complexos e custosos computacionalmente. %TODO: citar
Por esse motivo foi criado o conceito de objeto delimitador (\textit{bounding object}),
um objeto de geometria simples que é usado para conter os elementos que fazem
parte da simulação e que primeiro se verifica o contato entre esse objetos para, 
apenas se houver esse contato, fazer o cálculo do contato em si.

Apesar de adicionar uma etapa a mais no algoritmo, essa técnica é importante
pois trás dois benefícios: os algoritmos de detecção de contato criados
usando esse conceito podem
ser usados para objetos de qualquer geometria; e mais importante, esse técnica
reduz bastante o
tempo necessário para o processamento das simulações, podendo chegar a uma redução de
até 50\% do tempo de processamento nos casos de objetos com geometrias complexas,
uma vez que o cálculo de contato entre essas geometrias podem consumir elevado tempo de
processamento e o uso dos objetos delimitadores evita esse cálculo em casos 
onde o contato é impossível \citep{Munjiza2004, Ericson2005}.

\begin{figure}
	\centering
	\includegraphics[width=.5\linewidth]{Figuras/boundbox.png}
	\caption{Objeto delimitador circular (imagem retirada de \citet{Munjiza2004}}
	\label{fig:boundingbox}
\end{figure}

Nesse trabalho são usados objetos delimitadores esféricos, na figura \ref{fig:boundingbox}
podemos ver um exemplo desses objetos. O cálculo de contato é feito como mostrado na função \eqref{eq:contact}: 
comparando a distância entre o centro das esferas
com a soma de seus raios, se a inequação for verdadeira, há contato. 
Da equação \eqref{eq:contact} tiramos também que a complexidade computacional
dessa operação de comparação isolada é constante, ou $O(1)$.

\begin{equation} \label{eq:contact}
contato(A,B) = D_{AB} \leq R_A + R_B
\end{equation}
\listthisequation{\ref{eq:contact}}{Equação para determinação de contato.}

A solução ingênua de se implementar um algoritmo de
busca por contato é usando a função $contato()$ diretamente para cada dupla de elementos 
presentes no sistema. 
Esse algoritmo é chamado de Algoritmo de Checagem Direta, o algoritmo \ref{cod:dc1}
é sua versão sequencial e o \ref{cod:dc} é a versão paralela.

\begin{algorithm}
	\caption{Algoritmo de checagem direta}
	\label{cod:dc1}
	\begin{algorithmic}[1]
		\For{i de 0 a N}
			\For{j de i a N}
				\State{$contact(E_i, E_j)$}
			\EndFor
		\EndFor
	\end{algorithmic}
\end{algorithm}

\begin{algorithm}
	\caption{Algoritmo de checagem direta paralelo}
	\label{cod:dc}
	\begin{algorithmic}[1]
		\Paralelo
		\For{j de 0 a N}
			\State{$contact(E_i, E_j)$}
		\EndFor
		\Serial
	\end{algorithmic}
\end{algorithm}

A análise assintótica de ambos algoritmos é simples, visto que ambos constituem apenas de
estruturas de repetição e a única função usada possui complexidade 
constante. A equação \eqref{eq:dcbigo1} é a complexidade do Mapeamento Direto sequencial,
enquanto que a equação \eqref{eq:dcbigo2} é de sua versão paralela. É importante perceber
dois fatores sobre o algoritmo paralelo: o  algoritmo não se beneficia de um número de 
processadores maior que $N$ e que isso raramente acontecerá em uma simulação real, onde
milhões ou dezenas de milhões de objetos são usados, dessa maneira, quanto maior $N$ mais
próximo o algoritmo fica de complexidade quadrática.

\begin{align}
 MD  &= O(N^2)\label{eq:dcbigo1}\\
 MDP &= O\left( \left \lceil \frac{N}{P} \right \rceil \times N \right) \label{eq:dcbigo2}\\
 MDP &= O(N^2) & \text{se $P << N$}
\end{align}
\listthisequation{\ref{eq:dcbigo1}}{Complexidade do algoritmo de checagem direta.}

Com essa complexidade, grande número de objetos e a quantidade de vezes que essa etapa
precisa ser repetida durante uma simulação esse algoritmo se torna, na prática, inviável
\citep{Munjiza2004, Ericson2005}.

Para combater esse gargalo computacional é preciso diminuir ao máximo o número de
verificações de contato. Uma maneira de se fazer isso é deixando de verificar o contato
entre partículas que estejam demasiadamente longe umas das outras a ponto que a verificação
não seja necessária.

A classe de algoritmos que realiza essa operação é conhecida como Algoritmos de Busca
por Vizinhos. Esse algoritmos consistem em realizar um pré-mapeamento da localização
dos objetos para, após o mapeamento, testar o contato apenas entre os que estão próximos,
daí o nome de Busca por Vizinhos.

\subsection{Algoritmos de Busca por Vizinhos} 
\label{sec:vizinhos}

Os algoritmos de busca por vizinhos podem ser divididos em duas categorias. Algoritmos de árvore, são
aqueles que usam estruturas de árvores hierárquicas para representar o espaço de busca, como 
pode ser visto na Figura \ref{fig:three}; e 
algoritmos de \textit{grid} são os que separam o espaço de busca em células de igual tamanho.
Cada célula possui o lado de tamanho $d$ e pode ser endereçada
por coordenadas $x$,$y$ e $z$, assim como é visto  na Figura \ref{fig:grid}.

\begin{figure}
	\centering
	\subfigure[Divisão do espaço do tipo árvore]{\includegraphics[width=.5\linewidth]{Figuras/three.png}\label{fig:three}}\qquad
	\subfigure[Divisão do espaço do tipo grid]{\includegraphics[width=.4\linewidth]{Figuras/grid.png}\label{fig:grid}}
	\caption{Diferença na divisão do espaço de árvore para \textit{grid} (Figuras tiradas de \citet{Munjiza2004})}
\end{figure}

Para simulações de DEM temos que métodos de busca em \emph{grid} são mais eficientes
que os algoritmos baseados em árvores, principalmente quando tamanhos dos elementos
presentes na simulação é homogêneo \citep{Ericson2005, Han2007}. Além disso a estrutura
de \emph{grid} se assemelha à arquitetura paralela das \textsf{GPU}s, 
enquanto que a necessidade de reconstrução das árvores em cada passo faz com que
esse tipo de algoritmo perca sua eficiência quando aplicado ao \textsf{DEM},
que exige milhares de reconstruções durante uma mesma simulação \citep{Zheng2012}.
Dessa maneira esse trabalho foca seus experimentos na categoria de algoritmos de
\emph{grid}.

Esses algoritmos usam o modelo de \textit{grid}
para mapear a posição dos elementos no espaço de busca, mas se diferenciam
entre si na maneira escolhida para representar esse mapeamento na memória.
A diferença entre eles pode ser determinada pelas seguintes características:

\begin{itemize}
	\item A estrutura de dados usada para representar o \textit{grid};
	\item O tamanho escolhido para o lado da célula, representado nesse trabalho por $d$;
	\item Se um mesmo elemento pode ser mapeado para mais de uma célula.
\end{itemize}

Existem duas maneiras principais de se fazer a correspondência entre as coordenadas de 
um elemento e as do \emph{grid}. Pode-se atribuir cada elemento
a apenas uma célula, usando como base o ponto central do objeto delimitador do elemento.
Ou, se cada elemento pode pertencer a mais de uma célula, usamos um paralelepípedo que contenha
o objeto delimitador do elemento para determinar a faixa de células nas quais o devemos mapear.

Nos algoritmos do primeiro tipo, onde cada elemento só pode estar mapeado a uma
célula cada, é importante que o tamanho $d$ para a célula seja definido como o tamanho
do maior objeto delimitador do sistema. Assim é garantido que ao atribuir cada
elemento as suas células, nenhum poderá está em contato com um elemento a duas
células de distância. 

Nesses casos o cálculo para as posições $X, Y eZ$ do \emph{grid} é como apresentado nas equações
\eqref{eq:gx}, onde $(x_p, y_p, z_p)$ é a posição da partícula e
$(x_0, y_0, z_0)$ é a posição zero do espaço de busca. 
%Dessa maneira fazemos a função
%de mapeamento \ref{cod:mapeamento}, usado para calcular as coordenadas do \textit{grid}
%nos algoritmos de busca por vizinhos estudados.
%
\begin{align}
	\begin{split}
		\label{eq:gx} 
		X &= \left\lfloor\frac{x_p - x_0}{d}\right\rfloor\\
		Y &= \left\lfloor\frac{y_p - y_0}{d}\right\rfloor\\
		Z &= \left\lfloor\frac{z_p - z_0}{d}\right\rfloor
	\end{split}
\end{align} 
\listthisequation{\ref{eq:gx}}{Equações para coordenadas do \textit{grid}}

Se o segundo método é usado, as células que irão conter o mapeamento do elemento podem
ser obtidas percorrendo a partir da célula que contém o vértice inferior, esquerdo e atrás do paralelepípedo
até a que contém o vértice superior, direito e frente do mesmo. Para calcular esses dois pontos
basta usar a equação \eqref{eq:gy}, onde $r$ é o raio da esfera delimitadora.

\begin{align}
	\label{eq:gy} 
	X_0 &= \left\lfloor\frac{x_p - x_0}{d} - r\right\rfloor &  X_1 &= \left\lceil\frac{x_p - x_0}{d} + r\right\rceil \nonumber\\
	Y_0 &= \left\lfloor\frac{y_p - y_0}{d} - r\right\rfloor &  Y_1 &= \left\lceil\frac{y_p - y_0}{d} + r\right\rceil \\
	Z_0 &= \left\lfloor\frac{z_p - z_0}{d} - r\right\rfloor &  Z_1 &= \left\lceil\frac{z_p - z_0}{d} + r\right\rceil \nonumber
\end{align} 

Ambas as equações, \ref{eq:gx} e \ref{eq:gy}, possuem complexidade $O(1)$ e são usadas para
o mapeamento dos elementos no \emph{grid}. Como não há diferença entre elas em termo de complexidade,
a função $mapear(E)$ é usada para representar ambas nas análises dos algoritmos.

%Nesses casos a figura \ref{fig:vizinhas} ilustra as células
%vizinhas em volta da célula central. Um elemento na célula central só precisa ter
%o contato verificado com os elementos das células vizinhas e da própria célula central.

%O mapeamento dos elementos no \textit{grid} é feito usando suas coordenadas e o tamanho
%$d$ escolhido para as células. 

A técnica básica de \emph{grid} consiste em criar uma estrutura de dados onde
cada célula do \emph{grid} esteja representado diretamente por um \emph{array}, ou
uma lista encadeada, no
qual possamos colocar os elementos mapeados para aquela célula. Chamaremos
esse algoritmo de Mapeamento Direto (MD). 
Em sua versão paralela, para que não haja erro de concorrência,
a ação de adição de elemento em um \emph{array} tem que ser implementado como uma
operação atômica. Amba as versões, sequencial e paralela, possuem um alto custo de memória,
pois todo o \emph{grid} precisa ser representado como um \emph{array} multidimensional \citep{Munjiza2004}. 

A implementação \textsf{DEM} da \textsf{SDK CUDA}, o \emph{Particles} \citep{Green2013}, usa um \emph{array}
com os elementos ordenados de acordo com sua posição no \emph{grid}. Para evitar a busca
no vetor ordenado, ele usa também mais dois \emph{arrays} multidimensionais, com as mesmas
dimensões do \emph{grid}, onde são armazenados os índices referentes ao inicio e ao 
fim de cada célula no vetor ordenado \citep{Green2013}. 
Tal algoritmo foi inicialmente proposto por \citet{Kalojanov2009} 
que o aplicou para realizar traçado de raios (\emph{ray tracing}).
Chamaremos esse algoritmo de Mapeamento com Ordenação (MO).

O algoritmo de Mapeamento por Célula \citep{Araujo2007a, Cundall1988} é semelhante ao Mapeamento Direto, mas,
enquanto o MD necessita que o tamanho a célula $d$ seja equivalente ao diâmetro do
maior elemento do sistema, o Mapeamento por Célula (MC) permite escolher um tamanho 
arbitrário para $d$. Para que isso seja possível é preciso permitir que cada elemento
passe a ser mapeado para mais de uma célula no sistema.

\citet{Zheng2012} propõe a versão paralela do MC. 
Apesar de melhorar o desempenho,
em casos onde os elementos que compõe a simulação
possuem tamanhos muito variados, o custo de memória, assim como o próprio desempenho,
podem sofrer mudanças drásticas a depender do tamanho $d$ escolhido para célula.

A representação de todo o \emph{grid} de busca como um \emph{array} pode ter um
custo muito caro de memória. Uma alternativa é o algoritmo de Ordenação e Busca (OB), que
utiliza-se de vetores de tamanho $N$ para armazenar a posição dos elementos de maneira ordenada
e, na etapa de busca por contato, usa o algoritmo de Busca Binária para encontrar os elementos
das células vizinhas. Essa etapa de busca faz com que a busca por contato com os vizinhos tenha
complexidade $O(N \log (N) )$ e por esse motivo esse algoritmo não é muito popular na academia
\citep{Munjiza2004}.

Como não foi possível achar na literatura uma versão paralela desse algoritmo, 
nesse trabalho é proposta a
sua implementação paralela para comparação com os outros algoritmos paralelos, já que
sua versão sequencial já se mostrou eficiente em trabalhos passados \citep{Lins2011}.

O \emph{No Binary Search} (NBS) é um algoritmo que usa uma estrutura de lista encadeadas para 
representar o \emph{grid} \citep{Munjiza1998}, mas sem precisar da etapa de busca binária (daí o nome).
Para evitar a busca binária ele tem como requisito que a busca por contato seja feita de maneira
sequencial, em 
uma iteração sobre os elementos da simulação, e por isso não é possível ser paralelizado.

Apesar dos algoritmos apresentados não serem os únicos existentes na literatura, esses
são os mais gerais. Outros algoritmos, como por exemplo o apresentado por \citet{Harada2007}, que subdivide o 
espaço de busca em faixas , geralmente tem o uso muito restrito a configuração dos
cenários de busca \citep{Goswami2010} ou possuem um custo de memória tão alto como o MD, mas sem
ganho de desempenho \citep{Zheng2012}.

Em síntese, os principais algoritmos que podem ser aplicados de maneira geral a diferentes
cenários de busca são estes apresentados na tabela \ref{tab:algoritmos}. No próximo capitulo
cada um desses algoritmos é explicado em detalhes e analisado.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|}
		\hline
		\textbf{Algoritmo} & \textbf{Serial} & \textbf{Paralelo} \\
		\hline
		Mapeamento Direto & X & X \\
		\hline
		Mapeamento com Ordenação &  & X \\
		\hline
		Mapeamento por Célula & X & X \\
		\hline
		Ordenação e Busca & X & X \\
		\hline
		\textit{No Binary Search} & X & \\
		\hline
	\end{tabular}
	\caption{Pricipais algoritmos de busca por vizinhos}
	\label{tab:algoritmos}
\end{table}




%
%\begin{figure}
%	\centering
%	\includegraphics[width=.4\linewidth]{Figuras/vizinhos.png}
%	\caption{Célula central e as vizinhas em azul.}
%	\label{fig:vizinhas}
%\end{figure}
%


%O \emph{grid} terá suas dimensões definidas pelas equações \ref{eq:griddim}. 
%Onde $(x_f, y_f, z_f)$ 
%representa o valor máximo para $x$, $y$ e $z$. Esse valor pode ser definido 
%com a restrição do espaço de busca, ou a partir das posições das partículas do sistema.
%
%\begin{align}
%\begin{split}
%\label{eq:griddim} 
%D_X &= \left\lceil\frac{x_f - x_0}{d}\right\rceil\\
%%\label{eq:gy}
%D_Y &= \left\lceil\frac{y_f - y_0}{d}\right\rceil\\
%%\label{eq:gz}
%D_Z &= \left\lceil\frac{z_f - z_0}{d}\right\rceil
%\end{split}
%\end{align} 
%\listthisequation{\ref{eq:griddim}}{Equações para dimensão do \textit{grid} de busca}





