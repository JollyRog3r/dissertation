
\mychapter{\textsc{Experimento Numérico}}{ch:metodologia}
\lhead{\textsc{Experimento Numérico}}

Para responder as questões levantadas nesse trabalho, foi criado um experimento
numérico capaz de analisar cada um dos algoritmos
nos diferentes cenários de distribuição espacial.
A classificação desses cenários é feita de acordo com o agrupamento, ou empacotamento,
dos elementos presentes na simulação. Segue uma descrição dos diferentes tipos de empacotamento: 

\begin{description}
	\item[Empacotamento Denso:] durante todo tempo de simulação as partículas continuam sempre
	próximas umas das outras, 
	sua maioria em contato com ao menos uma outra partícula durante toda a simulação, normalmente
	associado ao estado sólido da matéria, um exemplo pode ser visto na Figura \ref{fig:denso};
	\item[Empacotamento Esparso:] as partículas entram raramente em contato nesse tipo de cenário
	normalmente elas ``vagam'' pelo espaço de busca durante boa parte da simulação sem encontrar
	outro elemento, normalmente associado ao estado gasoso da matéria, um exemplo pode ser
	visto na Figura \ref{fig:esparso};
	\item[Empacotamento Misto:] nesse tipo é possível encontrar, em diferentes
	grupos de partículas, as características tanto do empacotamento denso quanto do 
	esparso, esse empacotamento é normalmente associado ao estado líquido da matéria. A figura
	\ref{fig:misto} exemplifica esse tipo.
\end{description}

%Em geral, o melhor algoritmo a ser escolhido para um problema de busca vai depender
%das características do cenário de busca \citep{Munjiza2004, Ericson2005} no qual
%os elementos estão inseridos. Como foi
%visto em Algoritmos de Busca por Vizinhos (Sessão \ref{sec:algoritmos}), o comportamento
%desses algoritmos podem variar de acordo com a diferença de tamanhos dos 
%elementos envolvidos na simulação.

\begin{figure}
	\centering	
	\subfigure[Empacotamento Denso]{\includegraphics[width=.3\linewidth]{Figuras/dense_100k_2.png}\label{fig:denso}} 
	\subfigure[Empacotamento Esparso]{\includegraphics[width=.3\linewidth]{Figuras/sparse_5k_1.png}\label{fig:esparso}}
	\subfigure[Empacotamento Misto]{\includegraphics[width=.3\linewidth]{Figuras/fluid_50k_1.png}\label{fig:misto}}
	\caption{Exemplos dos três tipos de empacotamento (Figuras geradas pelo autor).}
	\label{fig:dynamic}
\end{figure}

Os testes dos algoritmos sequenciais foram apresentados em trabalho anterior pelo autor \citep{Lins2011} 
no qual foram comparados os resultados obtidos para cada algoritmo. Nesse trabalho o autor implementou
os algoritmos de Ordenação e Busca e o NBS no software DEMOOP \citep{DemoopTeorico}. 

\section{Implementação do Software Particles-Extended}

Para a realização dos experimentos em GPU, o autor desenvolveu um \emph{software} DEM
paralelo, nomeado \emph{Particles-Extended} (ou PEX).
O \textsf{PEX} foi baseado no \textit{software} \emph{Particles} 
\citep{Green2013}, mas com ênfase no desempenho e reuso do código.
Todos os algoritmos de detecção de contatos descritos nesse trabalho foram implementados
nesse \emph{software}. 

A implementação foi feita usando-se \textsf{C++} e 
a plataforma CUDA \citep{Nickolls2008}, para os algoritmos de busca foi usada a biblioteca
\emph{Thrust} \citep{Bell2012}, ambos disponíveis para download.
Todo o código fonte e casos de teste usados nesse
trabalho podem ser encontrados no repositório do autor no \emph{GitHub}\footnote{\url{https://github.com/jollyrog3r/pex}}.

Para simplificação do código e foco na detecção de contato o \textsf{PEX} possui apenas
a opção de geometria esférica para os elementos presentes na simulação e apenas o método de 
Euler foi adicionado, por sua simplicidade e baixo custo computacional. Por
esse motivo, também não é feito o cálculo do deslocamento rotacional das partículas.
Para o cálculo das forças de contatos é utilizado uma simplificação do método do plano
comum \citep{Cundall1988} onde a normal do plano é obtida a partir do vetor que liga
os pontos centrais das partículas que estão interagindo.

\section{Execução do Experimento}

Todos os testes foram executados em um \textit{cluster} computacional, cada computador com 
processador \textit{Intel Xeon E5-2670}
e uma placa \textsf{NVIDIA} \emph{Tesla M2075} com 6GB de memória e 448 núcleos de processamento.

Nas simulações foram usadas partículas de tamanho homogêneo. Todos algoritmos foram testados
usando-se as mesmas condições iniciais e os mesmos intervalos de $N$ para cada empacotamento, denso, esparso e misto. 
A variação do número de elementos para cada cenário pode ser vista na Tabela 
\ref{tab:tests}.

\begin{table}[h]
	\centering
	\begin{tabular}{|c|c|}
		\hline
		\textbf{Empacotamento} & \textbf{Número de Partículas}\\
		\hline
		Denso & $1\times 10^5$ a $5 \times 10^6$ \\
		\hline
		Esparso & $1\times 10^5$ a $10^6$ \\
		\hline
		Misto & $1\times 10^5$ a $10^6$ \\
		%\multirow{2}{*}{\textbf{Número de Partículas}}
		% & $5\times 10^5, 6\times 10^5, 7\times 10^5, 8\times 10^5, 9\times 10^5$\\
		% & $1\times 10^6, 2\times 10^6, 3\times 10^6, 4\times 10^6, 5\times 10^6$\\
		\hline
	\end{tabular}
	\caption{Quantidade de partículas por tipo de empacotamento}
	\label{tab:tests}
\end{table}

Cada simulação foi executada 14 vezes e a média do tempo total de execução foi calculada
para cada valor de $N$. Todas foram programadas para realizar 1500 passos no tempo, realizando
dessa maneira, na prática, 21 mil repetições de cada algoritmo,
para cada $N$ e em cada cenário diferente, de maneira a obter um baixo desvio padrão
nos resultados.

Para as simulações de empacotamento denso foi criado um cenário onde
todas as partículas se encontram dentro de um cubo fechado, em estado inicial de repouso.
Ao iniciar a simulação a força gravitacional 
tira as partículas do estado de repouso e elas caem até 
se estabilizarem. A Figura \ref{fig:densestates} demostra três momentos diferentes de uma
simulação, usando o \textsf{PEX}, com empacotamento denso e $10\times 10^5$ partículas.

\begin{figure}[h]
	\centering
	\subfigure[Estado inicial]{ \includegraphics[width=.3\linewidth]{Figuras/dense_100k_0.png}\label{fig:dense0}}\qquad
	\subfigure[Durante a simulação]{\includegraphics[width=.3\linewidth]{Figuras/dense_100k_1.png}\label{fig:dense1}}\qquad
	\subfigure[Estado final]{\includegraphics[width=.3\linewidth]{Figuras/dense_100k_2.png}\label{fig:dense2}}
	\caption{Estados da simulação do empacotamento denso}
	\label{fig:densestates}
\end{figure}

As simulações com empacotamento esparso foram feitas em um cenário similar,
com três mudanças:
a distância entre as partículas passa a ser de dez vezes o diâmetro das mesmas,
com uma pequena variação aleatória, a gravidade é diminuída para $\frac{1}{30}$ do valor original
e, ao invés de iniciarem em repouso, elas
possuem velocidade inicial com direção e intensidades também aleatórias. 
Dessa maneira, as partículas são lançadas em movimento 
para todas as direções desde o inicio e continuam assim durante todo tempo de simulação. 
A figura \ref{fig:sparsestates} apresenta três momentos diferentes de uma simulação com 
cinco mil partículas em empacotamento esparso.

\begin{figure}[h]
	\centering
	\subfigure[Estado inicial]{ \includegraphics[width=.3\linewidth]{Figuras/sparse_5k_0.png}\label{fig:sparse0}}\qquad
	\subfigure[Durante a simulação]{\includegraphics[width=.3\linewidth]{Figuras/sparse_5k_1.png}\label{fig:sparse1}}\qquad
	\subfigure[Estado final]{\includegraphics[width=.3\linewidth]{Figuras/sparse_5k_2.png}\label{fig:sparse2}}
	\caption{Estados da simulação do empacotamento esparso}
	\label{fig:sparsestates}
\end{figure}

O cenário mais diferente é o misto. Nesse caso os elementos partem de uma posição com densidade
alta, mas o espaço de busca configurado tem dimensões equivalentes ao esparso. Ao partir do 
repouso as partículas caem, puxadas pela gravidade, de encontro a um objeto
esférico posicionado abaixo das mesmas. Essa colisão espalha as partículas para todos
os lados enquanto elas continuam a cair, obtendo assim diferentes configurações de agrupamentos
na mesma simulação. A figura \ref{fig:mixedstates} mostra três momentos diferentes de uma simulação com 
cinquenta mil elementos em empacotamento esparso.

\begin{figure}[h]
	\centering
	\subfigure[Estado inicial]{ \includegraphics[width=.3\linewidth]{Figuras/fluid_50k_0.png}\label{fig:mixed0}}\qquad
	\subfigure[Durante a simulação]{\includegraphics[width=.3\linewidth]{Figuras/fluid_50k_1.png}\label{fig:mixed1}}\qquad
	\subfigure[Estado final]{\includegraphics[width=.3\linewidth]{Figuras/fluid_50k_2.png}\label{fig:mixed2}}
	\caption{Estados da simulação do empacotamento misto}
	\label{fig:mixedstates}
\end{figure}

%Já para a simulação do cenário misto foram feitas duas rampas. Inicialmente as partículas estão
%em repouso em cima da primeira rampa, quando a simulação é iniciada a gravidade faz com que
%as partículas deslizem rampa abaixo, caindo na segunda rampa e continuando a deslizar para baixo.
%Dessa maneira foi possível simular o espaço misto. O tempo total decorrido dentro da simulação é de
%5 segundos.

