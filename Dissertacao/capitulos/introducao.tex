
\mychapter{\textsc{Introdução}}{ch:introducao}
\lhead{\textsc{Introdução}}

Por mais de 50 anos a Lei de Moore \citep{MacK2011} assegurou o crescimento
do poder computacional, o que garantia uma certa segurança de que problemas
difíceis computacionalmente eventualmente passariam a poder ser resolvidos
pelo simples aumento de capacidade dos chips. Entretanto, está cada vez
mais difícil assegurar esse crescimento e 2017 será o ano em que, 
pela primeira vez desde sua formulação em 1975, a \textbf{Intel} não 
conseguirá acompanhar a Lei de Moore \citep{Mingis2016}.

Mas não é recente que a indústria vem encontrando obstáculos para continuar
nesse ritmo. Desde o início dos anos 2000 a dificuldade em aumentar o número de 
componentes dentro de um único núcleo de processamento fez com que 
outra abordagem surgisse: aumentar o número de núcleos de processamento
dentro do mesmo chip, popularizando assim os processadores \emph{multi-core} 
\citep{Asanovic2006, Feinbube2011}.

Estima-se que a \textsf{CPU} moderna terá até mil unidades de processamento
\citep{Asanovic2006}. Enquanto isso, cresce no mercado o número de linguagens de 
programação com suporte a paralelismo, assim como a demanda por profissionais 
com conhecimento nesse tipo de problema \citep{Feinbube2011}. Os fabricantes de
\textsf{GPU}, que anteriormente focavam seus produtos apenas na computação de elementos
gráficos, passam a fornecer placas capazes de realizar processamento 
massivamente paralelo,
além de fornecer as plataformas de desenvolvimento que as acompanham.

Segundo \citet{Nickolls2010} é chegada a era da \textsf{GPU}, onde essas
placas passaram a ser usadas, tanto pelo mercado quanto pela academia,
em problemas que necessitam de grande poder computacional e otimização.
%Neste trabalho abordamos um desses problemas: a detecção de contato em cenários
%de grande quantidades de elementos usando o Método dos Elementos Discretos.

Algoritmos de detecção de contatos são essenciais para diferentes áreas da computação
e engenharia  \citep{Munjiza2004, Ericson2005}.
Jogos digitais, computação gráfica, robótica, simulação, prototipagem e animações
computadorizadas são apenas alguns exemplos que não seriam possíveis sem detecção de contato. 
Mas esses algoritmos são considerados verdadeiros gargalos de seus sistemas,
em especial quando a etapa de busca por contato precisa ser realizada em tempo real,
ou mesmo quando o número de corpos envolvidos é muito grande, 
chegando a tomar de 60\% a 80\% do tempo
total de processamento \citep{Munjiza2004}.

Nesse trabalho o problema de detecção de contatos é abordado usando-se uma
ferramenta para simulação para cenários com grandes quantidades de objetos,
ou partículas.

O Método dos Elementos Discretos (\textsf{DEM}) \citep{Cundall1992} é um método
capaz de descrever o comportamento de um conjunto de corpos discretos
ao longo do tempo. Muito utilizado na engenharia, sua real utilidade
só é alcançada quando envolve centenas de milhares ou até milhões
de corpos em uma mesma simulação \citep{Munjiza2004}. Justamente o caso
em que a detecção de contatos passa a se tornar um problema.

A solução sequencial já foi bastante explorada, e atualmente
encontram-se algoritmos com tempo de processamento
$O(N)$ \citep{Munjiza2004} e com uso de memória também 
$O(N)$, para corpos de diâmetro similares \citep{Munjiza2004a,Chen2014}, ou 
para corpos de tamanhos distintos \citep{Araujo2007a,Perkins2001a}. Entretanto,
para a realização de uma simulação, esses algoritmos são repetidos dezenas
de milhares de vezes e isso, junto ao grande valor de $N$, torna a
solução sequencial muitas vezes inviável.

Como algumas etapas desse método é inerentemente paralela, o cálculo das forças
resultantes e a integração de movimento podem ser feitos para cada partícula
de maneira independente, é fácil ver como esse tipo de problema pode se
beneficiar de uma arquitetura massivamente paralela como da \textsf{GPU}.
A única dificuldade resulta da etapa de detecção de contato, que, por não ser
independente para cada partícula, não pode ser paralelizada de maneira direta.

Há na literatura diferentes algoritmos para essa etapa. \citet{Green2013} implementa
no \emph{Particles} uma versão direta de checagem
originalmente proposto por \citet{Kalojanov2009}. 
\citet{Zheng2012} propõe um versão diferente desse mesmo algoritmo modificando
apenas a maneira que cada elemento é mapeado. Ambos usam um vetor ordenado
para representar o \emph{grid} de busca com intenção de diminuir o alto custo de memória
exigido pelo algoritmo clássico de mapeamento direto \citep{Munjiza2004}.

Nesse trabalho é proposto a paralelização do 
algoritmo sequencial de detecção com ordenação e busca binária. 
O qual não havia ainda, até o momento da
realização desse trabalho, sido implementado de maneira paralela, 
pois, é visto como um algoritmo de baixa eficiência computacional
em arquitetura serial, devido às limitações dos algoritmos de ordenação e
buscas necessários para seu funcionamento \citep{Munjiza2004}.

A pergunta que motiva essa pesquisa é a seguinte: há um único 
algoritmo paralelo de busca por contatos que seja identificado como o mais
adaptado para as diferentes situações de busca?

Para encontrar a resposta foi feita a implementação paralela do Método dos Elementos
Discretos (\textsf{DEM}), que, em seguida, foi 
usado em um experimento empírico para comparar os algoritmos 
paralelos de busca por contatos encontrados na literatura.

Foi realizada uma análise quantitativa de cada algoritmo, comparando-se
a média do tempo de execução e a memória utilizada.
Também foram analisados os resultados obtidos em trabalhos anteriores para as 
versões sequenciais dos algoritmos.

Os resultados anteriores mostraram que, para algoritmo sequenciais, quanto mais
direta a representação do \emph{grid}, melhor. Mesmo usando mais memória esse
custo não era alto o suficiente para ser um impeditivo. Entretanto o algoritmo
de ordenação e busca binária obteve resultados competitivos \citep{Lins2011}.

Em contrapartida, para arquiteturas \textsf{GPU}, memória não é um recurso abundante
e a maneira como os dados são organizados possuem grande impacto no desempenho de algoritmos
paralelos.

A hipótese desse trabalho é que o algoritmo proposto, versão paralela do algoritmo 
com ordenação e busca, se mostre adaptado para os cenários estudados devido ao 
seu baixo custo de memória e à eficiência que sua versão sequencial já mostrou 
anteriormente.

%Em contrapartida, para arquiteturas \textsf{GPU}, memória não é um recurso abundante
%e a maneira como os dados são organizados possuem grande impacto no desempenho de algoritmos
%paralelos. Por esses motivos os resultados obtidos nesse trabalho diferem daqueles obtidos
%para suas versões sequenciais. Os algoritmos mais diretos, mas que usam mais memória, sequer
%foram capazes de realizar todos os testes devido a esse alto custo. Por outro lado,
%justamente o algoritmo de ordenação e busca, considerado lento por necessitar de etapas
%extras, foi o que obteve melhor desempenho, não apenas em relação ao custo de memória,
%mas também a tempo de execução.

%Muito trabalho tem sido feito no paralelismo de tais algoritmos, na tentativa de melhorar
%seus desempenho balanceando sua carga entre diferentes processadores \citep{Marzouk2005, Walther2009, Fattebert2012},
%mesmo assim a proporção de tempo de processamento necessário para detecção de contatos
%continua a mesma \citep{Fattebert2012}: de 60\% a 80\% do tempo
%total de processamento.

%Apesar dos principais algoritmos dessa categoria já terem sido bastante explorados 
%analiticamente, pouco estudo tem sido feito sobre como eles se diferenciam em variados
%cenários de simulação real \citep{Han2007}.
%
%O objetivo desse trabalho é preencher essa lacuna no que se refere ao comportamento
%dos principais algoritmos de busca por vizinhos %TODO Falar sobre busca de vizihos antes.
%em diferentes cenários de distribuição de partículas. Para isso o autor implementou esses algoritmos
%no \textit{software} \textsf{DEMOOP}, desenvolvido no Laboratório de Computação Científica e Visualização
%(\textsf{LCCV}) e os testou com diferentes cenários  de distribuição no \textit{cluster} 
%gradebr no mesmo laboratório.

%Diferentes algoritmos sequenciais se comportam de maneiras
%distintas dependendo dos diferentes tipos de distribuições espaciais
%das partículas presentes no espaço de busca \citep{Han2007,Lins2013}.
%Com esse conhecimento é possível escolher que tipo de algoritmo melhor
%se adapta à cada tipo de simulação. Escolha essa que pode ser feita
%inclusive em tempo real.
%
%Dependendo do cenário pode ser feita a escolha de qual algoritmo
%é mais eficiente para aquele caso, seja em relação ao tempo de processamento
%ou ao uso de memória, ou até mesmo à dificuldade de implementação de certos
%algoritmos. Sendo assim vê-se necessário o aprofundamento do conhecimento
%de como esses algoritmos paralelos se comportam em diferentes cenários.

%TODO sumário das conclusões e resultados

\section{Objetivo}

Essa dissertação tem dois objetivos gerais principais: propor a implementação
paralela do algoritmo de detecção com Ordenação e Busca Binária \citep{Munjiza2004}
que não foi realizada até agora e realizar uma análise comparativa
dos principais algoritmos paralelos, e sequenciais,
de busca por contato em diferentes cenários
de distribuição espacial de partículas. Buscando uma resposta sobre, 
como eles se 
comportam em diferentes cenários e se há um algoritmo
que seja o melhor adaptado em todas situações. Os objetivos podem ser
sintetizados como a seguir:

\subsubsection*{Identificar se o comportamento dos algoritmos paralelos são semelhantes aos seriais.}

Visto que muito já se conhece sobre os algoritmos de busca sequenciais, se os paralelos
se comportarem de maneira semelhante será possível saber previamente o que esperar desses.

\subsubsection*{Identificar se as características de empacotamento do cenário de busca influenciam no desempenho dos diferentes algoritmos.}

Como os algoritmos possuem diferentes necessidades de comunicação e organização
é possível que a mudança de características do cenário de busca influenciem 
negativamente ou positivamente de maneira individual para cada algoritmo.

\subsubsection*{Investigar a existência de um único algoritmo que seja o melhor adaptado em todos cenários, ou se cada cenário exige um diferente.}

A importância da descoberta de um algoritmo melhor em todas ocasiões é clara, visto que
com esse conhecimento fica sanada a dúvida de qual algoritmo implementar em uma
devida aplicação. Por outro lado, se as características do cenário influenciarem
os algoritmos a ponto de que para cada tipo de empacotamento existir um algoritmo
diferente ideal, abre-se a possibilidade futura de desenvolvimento de algoritmos paralelos
adaptativos, capazes de escolher para cada subdomínio do sistema o algoritmo que 
melhor se adéqua.

Esse conhecimento é importante pois, apesar de muitos algoritmos dessa área possuírem
a mesma complexidade assintótica, na prática, o custo médio de tempo de execução não
é calculado trivialmente, necessitando de análise empírica para sua descoberta. Outro 
fator importante a ser considerado é o custo de memória de cada algoritmo, que pode ser
fator chave para a escolha do algoritmo uma vez que esse recurso não é barato na arquitetura
de placas GPU.

\section{Metodologia}

Esse trabalho realiza uma pesquisa experimental numérica, que realiza os seguintes passos:

\begin{itemize}
	\item Implementação de um \emph{software} DEM paralelo;
	\item Experimento quantitativo dos diferentes algoritmos estudados;
	\item Análise de desempenho face os cenários distintos.
\end{itemize}

\section{Estrutura da Dissertação}

Essa dissertação está organizada em 6 capítulos, são eles:

Capitulo~\ref{ch:introducao}: introdução ao tema, motivação, objetivo, questão da pesquisa 
e estrutura da dissertação.

Capitulo~\ref{ch:fundamentacao}: fundamentação teórica do trabalho. Apresentação do problema de
detecção de contatos e do Método dos Elementos Discretos.

Capitulo~\ref{ch:algoritmos}: apresentação e análise dos algoritmos de busca por vizinhos.

Capitulo~\ref{ch:metodologia}: apresentação da metodologia seguida durante a realização
do experimento, assim como das ferramentas utilizadas.

Capitulo~\ref{ch:resultados}: resultados obtidos após a execução dos experimentos.

Capitulo~\ref{ch:consideracoes}: análise dos resultados e objetivos alcançados
do trabalho, assim como perspectivas futuras.