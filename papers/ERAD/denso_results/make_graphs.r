
library(jsonlite)
library(lattice)

# Lê os dados do arquivo
data <- fromJSON(file("data.json"))

#separa os algorítimos
nbs = data$nbs
sort = data$sort
dm = data$dm

#reordena por N
nbs = nbs[order(nbs$N),]
sort = sort[order(sort$N),]
dm = dm[order(dm$N),]

nbs$mean = sapply(nbs$results, mean)
sort$mean = sapply(sort$results, mean)
dm$mean = sapply(dm$results, mean)

nbs$var = sapply(nbs$results, var)
sort$var = sapply(sort$results, var)
dm$var = sapply(dm$results, var)

print("NBS")
print(nbs)
print("Sort")
print(sort)
print("DM")
print(dm)

png(filename="result.png", height=400, width=700, 
    bg="white")

max_y = max(c(na.omit(nbs$mean), na.omit(dm$mean), na.omit(sort$mean)))

plot(x=dm$N, y=dm$mean, col="blue", type="o", ylim = c(0,max_y),
     xlab = "N", ylab = "Segundos", pch=21, lty=1)
lines(x=nbs$N, y=nbs$mean, type="o", pch=22, lty=2, col="red")
lines(x=sort$N, y=sort$mean, type="o", pch=23, lty=3, col="purple")

title(main="Desempenho - Espaçamento Denso")
legend(500000, max_y, c("DM", "NBS", "Sorting"), cex=0.8, col=c("blue", "red", "purple"), 
       pch=21:23, lty=1:3);

nbs_dm = mean(nbs$mean / dm$mean * 100, na.rm = TRUE)
sort_dm = mean((dm$mean - sort$mean) / dm$mean * 100, na.rm = TRUE)

dev.off()
