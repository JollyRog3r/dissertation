# -*- coding: utf-8 -*-

import os
import re
import json

from datetime import timedelta

data = {}

for root, dirs, files in os.walk('.'):
    if root == '.':
        for dir in dirs:
            data[dir] = {}
    else:
        tag = os.path.basename(root)
        data[tag] = []

        results = {}
        for f in files:
            num = int(re.split('\.|-', f)[0])
            if not num in results:
                results[num] = []
            the_file = os.path.join(root, f)
            for line in reversed(open(the_file).readlines()):
                if 'done' in line:
                    elapsed = line.split()[3]
                    t1 = elapsed
                    elapsed = re.findall('\d+', elapsed)
                    elapsed = timedelta(hours=int(elapsed[0]),
                                        minutes=int(elapsed[1]),
                                        seconds=int(elapsed[2]))
                    t2 = elapsed
                    results[num].append(elapsed.total_seconds())

        for n, r in results.iteritems():
            data[tag].append({"N" : n, "results": r})


print json.dumps(data, indent=4, sort_keys=True)
